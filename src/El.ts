/**************************
*. BASIC HELPER FUNCTIONS
***************************/

export function byId(id): HTMLElement { return document.getElementById(id); }

// adds and removes classes.  can take a string with spaces for mutiple classes
export function classSplice(
	element: HTMLElement,
	removeClasses: string | string[],
	addClasses: string | string[],
): HTMLElement {
	if (removeClasses) {
		const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
		remList.forEach((className) => element.classList.remove(className));
	}
	if (addClasses) {
		const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
		addList.forEach((className) => element.classList.add(className));
	}
	return element;
}
export function addClass(element: HTMLElement, addClasses: string): HTMLElement {
	return classSplice(element, undefined, addClasses);
}

// remove all children from element
export function removeChildren(parent: HTMLElement) {
	let child = parent.firstChild;
	while( !!child ) {
		parent.removeChild(child);
		child = parent.firstChild;
	}
}

export function setStyle(root: HTMLElement, style: {[key in keyof CSSStyleDeclaration]?: string} | string) {
	if (typeof style === "string") {
		root.style.cssText = style;
	} else {
		Object.keys(style).forEach((styleName) => root.style[styleName] = style[styleName]);
	}
}

/**************************
*. TYPES / INTERFACES
***************************/

export type RawHTML = {hackableHTML: string};
export type Child = string | HTMLElement | number | null | undefined | RawHTML | (() => Child);
export type Innards = Child | Array<Child> | (() => Innards);

export interface IProps {
	class?: string;
	className?: string;
	classList?: string[];
	text?: string;
	innerHTML?: string;
	id?: string;
	attr?: {[key: string]: string};
	style?: {[key: string]: string} | string;
	on?: {[key: string]: (event: Event) => boolean | void | any};
	onClick?: (event: MouseEvent) => boolean | void | any;
	onMouseDown?: (event: MouseEvent) => boolean | void | any;
	onKeyDown?: (event: KeyboardEvent) => boolean | void | any;
	prependTo?: HTMLElement;
	appendTo?: HTMLElement;
	this?: {
		[key: string]: any;
	}
	innards?: Innards;
}

/**************************
*. TYPES CHECKS
***************************/

export function isHTMLElement(testMe: any): testMe is HTMLElement {
	return testMe instanceof HTMLElement;
}

export function isRawHTML(testMe: any): testMe is RawHTML {
	return typeof testMe === "object" && testMe.hasOwnProperty("hackableHTML");
}

export function isProps(testMe: any): testMe is IProps {
	return typeof testMe === "object" && !Array.isArray(testMe) && !isHTMLElement(testMe) && !isRawHTML(testMe);
}



/*************************
*. MAIN CREATION FN
*************************/

export function create<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	const input = standardizeInput(null, propsOrInnards, innards);
	return privateCreate(tagName, input.props, input.innards);
}


function standardizeInput(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): {
	props: IProps,
	innards: Innards,
} {
	let props: IProps;
	if (classNameOrProps && isProps(classNameOrProps)) {
		props = classNameOrProps;
	} else if (propsOrInnards && isProps(propsOrInnards)) {
		props = propsOrInnards;
	} else {
		props = {};
	}

	if (propsOrInnards && !isProps(propsOrInnards)) {
		if (innards !== undefined) { throw new Error("Can not double define innards"); }
		innards = propsOrInnards;
	}

	if (typeof classNameOrProps === "string") {
		props.className = (props.className ? props.className + " " : "") + classNameOrProps;
	}
	return {props, innards};
}


function privateCreate<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	props?: IProps,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	const out = document.createElement(tagName);

	if (props) {
		// event stuff
		if (props.on) {
			const eventListeners = props.on || {};
			Object.keys(eventListeners).forEach((eventType) =>
				out.addEventListener(eventType, eventListeners[eventType])
			);
		}
		if (props.onClick) { out.addEventListener("click", props.onClick); }
		if (props.onKeyDown) { out.addEventListener("keydown", props.onKeyDown); }
		if (props.onMouseDown) { out.addEventListener("mousedown", props.onMouseDown); }

		// element html props, and element js props
		if (props.attr) {
			const attrs = props.attr || {};
			Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
		}
		if (props.this) {
			const thisProps = props.this || {};
			Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
		}

		// prop style content
		if (props.text) { out.textContent += props.text; }
		if (props.innerHTML) { out.innerHTML += props.innerHTML; }

		// classing
		const classList = props.classList || [];
		if (props.className) { classList.push(props.className) }
		if (props.class) { classList.push(props.class) }
		out.className =  classList.join(" ");
		if (props.id) { out.id = props.id; }


		// inline styles
		if (props.style) { setStyle(out, props.style); }

		// innards alternative
		if (props.innards) { append(out, props.innards); }
	}

	// add all chidren, or innards
	if (innards) { append(out, innards); }

	// append to parent at the end, so multiple reflows are not triggered
	if (props) {
		if (props.appendTo) {
			props.appendTo.appendChild(out);
		}
		if (props.prependTo) {
			const parent = props.prependTo;
			parent.insertBefore(out, parent.firstChild);
		}
	}

	return out;
}


export function setInnards(root: HTMLElement, innards: Innards) {
	removeChildren(root);
	append(root, innards);
}

// Append stuff to an element.
// Takes A. a single string as innerText,
// or B. an array consisting of
// 1. strings, numbers for innerText
// 2. null, undefined for ignoring
// 3. a RawHTML object which ensures users know of vulnerabilities while modifying innerHTML
// 4. HTMLElements to appendChild
// 5. A function which returns any of the above
export function append(
	root: HTMLElement,
	innards: Innards,
) {
	if (innards === undefined || innards === null) { return }
	while (typeof innards === "function") { innards = innards(); }
	let childList = Array.isArray(innards) ? innards : [innards];
	childList.forEach((child) => {
		if (typeof child === "function") { child = child(); }
		if (child === undefined || child === null) { return; }
		if (typeof child === "number") { child = child + ""; }
		if (isRawHTML(child)) {
			root.innerHTML += child.hackableHTML;
		} else if (typeof child === "string") {
			root.appendChild(new Text(child))
		} else {
			root.appendChild(child as any); 
		}
	});
}

/*****************************
* REGULAR CREATION SHORTHANDS
******************************/

export function hr() { return document.createElement("hr"); }
export function br() { return document.createElement("br"); }

export function div(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLDivElement {
	const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
	return create("div", input.props, input.innards);
}

export function span(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLSpanElement {
	const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
	return create("span", input.props, input.innards);
}

export interface IAnchorProps extends IProps { href?: string; }
export function a(
	classNameOrProps?: string | IAnchorProps,
	propsOrInnards?: IAnchorProps | Innards,
	innards?: Innards,
): HTMLAnchorElement {
	const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
	input.props.attr = input.props.attr || {};
	input.props.attr.href = (input.props as IAnchorProps).href;
	return create("a", input.props, input.innards);
}


export abstract class SimpleComponent <EL_TYPE extends HTMLElement = HTMLElement> {
	protected _domNode: EL_TYPE;
	public get domNode(): EL_TYPE {
		if (this._domNode === undefined) {
			this._domNode = this.render();
			if (this._domNode === undefined) {
				throw new Error("Component._domNode must not be undefined after render()")
			}
		}
		return this._domNode;
	}

	protected abstract render(): EL_TYPE;
}